package com.codeupanaya.springmvctutorials.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/greeting")
public class MainController {	
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView greet() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("helloworld");
		model.addObject("message", "hello");
		
		return model; 
	}
}